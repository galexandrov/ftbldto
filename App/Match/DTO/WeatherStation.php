<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class WeatherStation
{
    /**
     * @Type("string")
     * @SerializedName("name")
     */
    private $_name;
    /**
     * @Type("float")
     * @SerializedName("lat")
     */
    private $_lat;
    /**
     * @Type("float")
     * @SerializedName("lng")
     */
    private $_lng;

    public function __construct($name=null)
    {
        $this->setName($name);
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setLat($lat)
    {
        $this->_lat = $lat;
    }

    public function getLat()
    {
        return $this->_lat;
    }

    public function setLng($lng)
    {
        $this->_lng = $lng;
    }

    public function getLng()
    {
        return $this->_lng;
    }
}
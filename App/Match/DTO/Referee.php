<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class Referee
{
    /**
     * @Type("string")
     * @SerializedName("first_name")
     */
    private $_firstName;
    /**
     * @Type("string")
     * @SerializedName("last_name")
     */
    private $_lastName;

    public function __construct($firstName, $lastName)
    {
        $this->setFirstName($firstName);
        $this->setLastName($lastName);
    }

    public function setFirstName($firstName)
    {
        $this->_firstName = $firstName;
    }

    public function getFirstName()
    {
        return $this->_firstName;
    }

    public function setLastName($lastName)
    {
        $this->_lastName = $lastName;
    }

    public function getLastName()
    {
        return $this->_lastName;
    }
}
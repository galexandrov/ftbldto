<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class League
{
    /**
     * @Type("string")
     * @SerializedName("name")
     */
    private $_name;
    /**
     * @Type("string")
     * @SerializedName("shortname")
     */
    private $_shortname;
    /**
     * @Type("string")
     * @SerializedName("reference")
     */
    private $_reference;
    /**
     * @Type("string")
     * @SerializedName("description")
     */
    private $_description;
    /**
     * @Type("string")
     * @SerializedName("logo")
     */
    private $_logo;
    /**
     * @Type("string")
     * @SerializedName("twitter")
     */
    private $_twitter;
    /**
     * @Type("string")
     * @SerializedName("facebook")
     */
    private $_facebook;
    /**
     * @Type("string")
     * @SerializedName("website")
     */
    private $_website;
    /**
     * @Type("integer")
     * @SerializedName("oneYear")
     */
    private $_oneYear;
    /**
     * @Type("integer")
     * @SerializedName("oneYearUnder")
     */
    private $_oneYearUnder;
    /**
     * @Type("integer")
     * @SerializedName("enabled")
     */
    private $_enabled;
    /**
     * @Type("Ftbl\Match\DTO\Country")
     * @SerializedName("country")
     */
    private $_country;

    public function __construct($name=null)
    {
        $this->setName($name);
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setShortname($shortname)
    {
        $this->_shortname = $shortname;
    }

    public function getShortname()
    {
        return $this->_shortname;
    }

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setDescription($description)
    {
        $this->_description = $description;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function setLogo($logo)
    {
        $this->_logo = $logo;
    }

    public function getLogo()
    {
        return $this->_logo;
    }

    public function setTwitter($twitter)
    {
        $this->_twitter = $twitter;
    }

    public function getTwitter()
    {
        return $this->_twitter;
    }

    public function setFacebook($facebook)
    {
        $this->_facebook = $facebook;
    }

    public function getFacebook()
    {
        return $this->_facebook;
    }

    public function setWebsite($website)
    {
        $this->_website = $website;
    }

    public function getWebsite()
    {
        return $this->_website;
    }

    public function setOneYear($oneYear)
    {
        $this->_oneYear = $oneYear;
    }

    public function getOneYear()
    {
        return $this->_oneYear;
    }

    public function setOneYearUnder($oneYearUnder)
    {
        $this->_oneYearUnder = $oneYearUnder;
    }

    public function getOneYearUnder()
    {
        return $this->_oneYearUnder;
    }

    public function setCountry(Country $country)
    {
        $this->_country = $country;
    }

    public function getCountry()
    {
        return $this->_country;
    }

    public function setEnabled($enabled)
    {
        $this->_enabled = $enabled;
    }

    public function getEnabled()
    {
        return $this->_enabled;
    }

    public function toJson()
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($this, 'json');
    }

    public function fromJson($match)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->deserialize($match, 'Ftbl\Match\DTO\League', 'json');
    }

}
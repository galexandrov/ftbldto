<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class Stadium
{
    /**
     * @Type("string")
     * @SerializedName("name")
     */
    private $_name;
    /**
     * @Type("Ftbl\Match\DTO\City")
     * @SerializedName("city")
     */
    private $_city;
    /**
     * @Type("string")
     * @SerializedName("reference")
     */
    private $_reference;
    /**
     * @Type("float")
     * @SerializedName("lat")
     */
    private $_lat;
    /**
     * @Type("float")
     * @SerializedName("lng")
     */
    private $_lng;
    /**
     * @Type("Ftbl\Match\DTO\WeatherStation")
     * @SerializedName("weather_station")
     */
    private $_weatherStation;

    public function __construct($name=null)
    {
        $this->setName($name);
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setCity($city)
    {
        $this->_city = $city;
    }

    public function getCity()
    {
        return $this->_city;
    }

    public function setLat($lat)
    {
        $this->_lat = $lat;
    }

    public function getLat()
    {
        return $this->_lat;
    }

    public function setLng($lng)
    {
        $this->_lng = $lng;
    }

    public function getLng()
    {
        return $this->_lng;
    }

    public function setWeatherStation($weatherStation)
    {
        $this->_weatherStation = $weatherStation;
    }

    public function getWeatherStation()
    {
        return $this->_weatherStation;
    }

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function toJson()
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($this, 'json');
    }

    public function fromJson($match)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->deserialize($match, 'Ftbl\Match\DTO\Stadium', 'json');
    }
}
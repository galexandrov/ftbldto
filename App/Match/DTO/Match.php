<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;


class Match
{

    /**
     * @Type("string")
     * @SerializedName("reference")
     */
    private $_reference;
    /**
     * @Type("string")
     * @SerializedName("kickoff")
     */
    private $_kickoff;
    /**
     * @Type("Ftbl\Match\DTO\Team")
     * @SerializedName("homeTeam")
     */
    private $_homeTeam;
    /**
     * @Type("Ftbl\Match\DTO\Team")
     * @SerializedName("awayTeam")
     */
    private $_awayTeam;
    /**
     * @Type("Ftbl\Match\DTO\Result")
     * @SerializedName("ftResult")
     */
    private $_ftResult;
    /**
     * @Type("Ftbl\Match\DTO\Result")
     * @SerializedName("htResult")
     */
    private $_htResult;
    /**
     * @Type("Ftbl\Match\DTO\Result")
     * @SerializedName("penaltyResult")
     */
    private $_penaltyResult;
    /**
     * @Type("Ftbl\Match\DTO\Result")
     * @SerializedName("etResult")
     */
    private $_etResult;
    /**
     * @Type("Ftbl\Match\DTO\Result")
     * @SerializedName("aggResult")
     */
    private $_aggResult;
    /**
     * @Type("Ftbl\Match\DTO\League")
     * @SerializedName("league")
     */
    private $_league;
    /**
     * @Type("Ftbl\Match\DTO\Season")
     * @SerializedName("season")
     */
    private $_season;
    /**
     * @Type("Ftbl\Match\DTO\Stadium")
     * @SerializedName("stadium")
     */
    private $_stadium;
    /**
     * @Type("Ftbl\Match\DTO\Round")
     * @SerializedName("round")
     */
    private $_round;
    /**
     * @Type("integer")
     * @SerializedName("attendance")
     */
    private $_attendance;
    /**
     * @Type("Ftbl\Match\DTO\Referee")
     * @SerializedName("referee")
     */
    private $_referee;
    /**
     * @Type("Ftbl\Match\DTO\Team")
     * @SerializedName("winnerTeam")
     */
    private $_winnerTeam;
    /**
     * @Type("boolean")
     * @SerializedName("locked")
     */
    private $_locked;
    // private $_season;

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setKickoff($kickoff)
    {
        $this->_kickoff = $kickoff;
    }

    public function getKickoff()
    {
        return $this->_kickoff;
    }

    public function setHomeTeam(Team $homeTeam)
    {
        $this->_homeTeam = $homeTeam;
    }

    public function getHomeTeam()
    {
        return $this->_homeTeam;
    }

    public function setAwayTeam(Team $awayTeam)
    {
        $this->_awayTeam = $awayTeam;
    }

    public function getAwayTeam()
    {
        return $this->_awayTeam;
    }

    public function setLeague(League $league)
    {
        $this->_league = $league;
    }

    public function getLeague()
    {
        return $this->_league;
    }

    public function setFtResult(Result $ftResult)
    {
        $this->_ftResult = $ftResult;
    }

    public function getFtResult()
    {
        return $this->_ftResult;
    }

    public function setHtResult(Result $htResult)
    {
        $this->_htResult = $htResult;
    }

    public function getHtResult()
    {
        return $this->_htResult;
    }

    public function getPenaltyResult()
    {
        return $this->_penaltyResult;
    }

    public function setPenaltyResult(Result $penaltyResult)
    {
        $this->_penaltyResult = $penaltyResult;
    }

    public function getEtResult()
    {
        return $this->_etResult;
    }

    public function setEtResult(Result $etResult)
    {
        $this->_etResult = $etResult;
    }

    public function setAggResult(Result $aggResult)
    {
        $this->_aggResult = $aggResult;
    }

    public function getAggResult()
    {
        return $this->_aggResult;
    }

    public function setRound(Round $round)
    {
        $this->_round = $round;
    }

    public function getRound()
    {
        return $this->_round;
    }

    public function setStadium(Stadium $stadium)
    {
        $this->_stadium = $stadium;
    }

    public function getStadium()
    {
        return $this->_stadium;
    }

    public function setSeason(Season $season)
    {
        $this->_season = $season;
    }

    public function getSeason()
    {
        return $this->_season;
    }

    public function setLocked($locked)
    {
        $this->_locked = $locked;
    }

    public function getLocked()
    {
        return $this->_locked;
    }

    public function setReferee(Referee $referee)
    {
        $this->_referee = $referee;
    }

    public function getReferee()
    {
        return $this->_referee;
    }

    public function setWinnerTeam(Team $winnerTeam)
    {
        $this->_winnerTeam = $winnerTeam;
    }

    public function getWinnerTeam()
    {
        return $this->_winnerTeam;
    }

    public function setAttendance($attendance)
    {
        $this->_attendance = $attendance;
    }

    public function getAttendance()
    {
        return $this->_attendance;
    }

    public function toJson()
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($this, 'json');
    }

    public function fromJson($match)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->deserialize($match, 'Ftbl\Match\DTO\Match', 'json');
    }
}
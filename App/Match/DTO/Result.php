<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class Result
{
    /**
     * @Type("integer")
     * @SerializedName("home")
     */
    private $_homeGoals;
    /**
     * @Type("integer")
     * @SerializedName("away")
     */
    private $_awayGoals;

    public function __construct($homeGoals, $awayGoals)
    {
        $this->setHome($homeGoals);
        $this->setAway($awayGoals);
    }

    public function setHome($homeGoals)
    {
        $this->_homeGoals = $homeGoals;
    }

    public function getHome()
    {
        return $this->_homeGoals;
    }

    public function setAway($awayGoals)
    {
        $this->_awayGoals = $awayGoals;
    }

    public function getAway()
    {
        return $this->_awayGoals;
    }
}
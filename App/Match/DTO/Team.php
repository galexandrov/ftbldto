<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class Team
{
    /**
     * @Type("string")
     * @SerializedName("name")
     */
    private $_name;
    /**
     * @Type("string")
     * @SerializedName("full_name")
     */
    private $_full_name;
    /**
     * @Type("string")
     * @SerializedName("reference")
     */
    private $_reference;
    /**
     * @Type("string")
     * @SerializedName("logo")
     */
    private $_logo;
    /**
     * @Type("string")
     * @SerializedName("description")
     */
    private $_description;
    /**
     * @Type("string")
     * @SerializedName("wiki_page")
     */
    private $_wiki_page;
    /**
     * @Type("string")
     * @SerializedName("website")
     */
    private $_website;
    /**
     * @Type("string")
     * @SerializedName("twitter")
     */
    private $_twitter;
    /**
     * @Type("string")
     * @SerializedName("facebook")
     */
    private $_facebook;
    /**
     * @Type("string")
     * @SerializedName("teamcolor")
     */
    private $_teamcolor;
    /**
     * @Type("string")
     * @SerializedName("tm_id")
     */
    private $_tm_id;
    /**
     * @Type("Ftbl\Match\DTO\City")
     * @SerializedName("city")
     */
    private $_city;
    /**
     * @Type("Ftbl\Match\DTO\Stadium")
     * @SerializedName("stadium")
     */
    private $_stadium;
    /**
     * @Type("array")
     * @SerializedName("aliases")
     */
    private $_aliases = array();

    public function __construct($name=null)
    {
        $this->setName($name);
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setTmId($tm_id)
    {
        $this->_tm_id = $tm_id;
    }

    public function getTmId()
    {
        return $this->_tm_id;
    }

    public function setFullName($fullName)
    {
        $this->_full_name = $fullName;
    }

    public function getFullName()
    {
        return $this->_full_name;
    }

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setStadium(Stadium $stadium)
    {
        $this->_stadium = $stadium;
    }

    public function getStadium()
    {
        return $this->_stadium;
    }

    public function setLogo($logo)
    {
        $this->_logo = $logo;
    }

    public function getLogo()
    {
        return $this->_logo;
    }

    public function setDescription($description)
    {
        $this->_description = $description;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function setWikiPage($wikiPage)
    {
        $this->_wiki_page = $wikiPage;
    }

    public function getWikiPage()
    {
        return $this->_wiki_page;
    }

    public function setWebsite($website)
    {
        $this->_website = $website;
    }

    public function getWebsite()
    {
        return $this->_website;
    }

    public function setTwitter($twitter)
    {
        $this->_twitter = $twitter;
    }

    public function getTwitter()
    {
        return $this->_twitter;
    }

    public function setFacebook($facebook)
    {
        $this->_facebook = $facebook;
    }

    public function getFacebook()
    {
        return $this->_facebook;
    }

    public function setCity(City $city)
    {
        $this->_city = $city;
    }

    public function getCity()
    {
        return $this->_city;
    }

    public function getTeamColor()
    {
        return $this->_teamcolor;
    }

    public function setTeamColor($teamcolor)
    {
        $this->_teamcolor = $teamcolor;
    }

    public function getAliases()
    {
        return $this->_aliases;
    }

    public function setAliases($aliases)
    {
        $this->_aliases = $aliases;
    }

    public function toJson()
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($this, 'json');
    }

    public function fromJson($match)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->deserialize($match, 'Ftbl\Match\DTO\Team', 'json');
    }

}
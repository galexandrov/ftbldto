<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class TeamAlias
{
    /**
     * @Type("string")
     * @SerializedName("name")
     */
    private $_name;
    /**
     * @Type("string")
     * @SerializedName("type")
     */
    private $_type;
    /**
     * @Type("string")
     * @SerializedName("team")
     */
    private $_team;

    public function __construct($name=null)
    {
        $this->setName($name);
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setType($type)
    {
        $this->_type = $type;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function getTeam()
    {
        return $this->_team;
    }

    public function setTeam($team)
    {
        $this->_team = $team;
    }

    public function toJson()
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($this, 'json');
    }

    public function fromJson($match)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->deserialize($match, 'Ftbl\Match\DTO\TeamAlias', 'json');
    }

}
<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class LeagueRules
{
    /**
     * @Type("string")
     * @SerializedName("league")
     */
    private $_league;
    /**
     * @Type("integer")
     * @SerializedName("year")
     */
    private $_year;
    /**
     * @Type("integer")
     * @SerializedName("pointsWin")
     */
    private $_pointsWin;
    /**
     * @Type("array")
     * @SerializedName("rules")
     */
    private $_rules;

    public function __construct()
    {

    }

    public function setLeague($league)
    {
        $this->_league = $league;
    }

    public function getLeague()
    {
        return $this->_name;
    }

    public function setYear($year)
    {
        $this->_year = $year;
    }

    public function getYear()
    {
        return $this->_year;
    }

    public function setRules($rules)
    {
        $this->_rules = $rules;
    }

    public function getRules()
    {
        return $this->_rules;
    }

    public function setPointsWin($pointsWin)
    {
        $this->_pointsWin = $pointsWin;
    }

    public function getPointsWin()
    {
        return $this->_pointsWin;
    }

    public function toJson()
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($this, 'json');
    }

    public function fromJson($match)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->deserialize($match, 'Ftbl\Match\DTO\LeagueRules', 'json');
    }
}
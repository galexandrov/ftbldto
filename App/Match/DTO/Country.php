<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class Country
{
    /**
     * @Type("string")
     * @SerializedName("reference")
     */
    private $_reference;
    /**
     * @Type("string")
     * @SerializedName("name")
     */
    private $_name;
    /**
     * @Type("string")
     * @SerializedName("nationality")
     */
    private $_nationality;
    /**
     * @Type("string")
     * @SerializedName("iso2")
     */
    private $_iso2;
    /**
     * @Type("string")
     * @SerializedName("iso3")
     */
    private $_iso3;
    /**
     * @Type("string")
     * @SerializedName("federation")
     */
    private $_federation;
    /**
     * @Type("string")
     * @SerializedName("federation_continent")
     */
    private $_federation_continent;

    public function __construct($name=null)
    {
        $this->setName($name);
    }

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setNationality($nationality)
    {
        $this->_nationality = $nationality;
    }
    public function getNationality()
    {
        return $this->_nationality;
    }

    public function setIso2($iso2)
    {
        $this->_iso2 = $iso2;
    }
    public function getIso2()
    {
        return $this->_iso2;
    }

    public function setIso3($iso3)
    {
        $this->_iso3 = $iso3;
    }
    public function getIso3()
    {
        return $this->_iso3;
    }

    public function setFederation($federation)
    {
        $this->_federation = $federation;
    }
    public function getFederation()
    {
        return $this->_federation;
    }

    public function setFederationContinent($federation_continent)
    {
        $this->_federation_continent = $federation_continent;
    }
    public function getFederationContinent()
    {
        return $this->_federation_continent;
    }

    public function toJson()
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($this, 'json');
    }

    public function fromJson($match)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->deserialize($match, 'Ftbl\Match\DTO\Country', 'json');
    }

}
<?php
namespace Ftbl\Match\DTO;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class Season
{
    /**
     * @Type("string")
     * @SerializedName("name")
     */
    private $_name;
    /**
     * @Type("string")
     * @SerializedName("reference")
     */
    private $_reference;
    /**
     * @Type("string")
     * @SerializedName("start")
     */
    private $_start;
    /**
     * @Type("string")
     * @SerializedName("end")
     */
    private $_end;

    public function __construct($name)
    {
        $this->setName($name);
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setStart($start)
    {
        $this->_start = $start;
    }

    public function getStart()
    {
        return $this->_start;
    }

    public function setEnd($end)
    {
        $this->_end = $end;
    }

    public function getEnd()
    {
        return $this->_end;
    }

    public function toJson()
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($this, 'json');
    }

    public function fromJson($match)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->deserialize($match, 'Ftbl\Match\DTO\Season', 'json');
    }
}
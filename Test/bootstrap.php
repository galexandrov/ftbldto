<?php
include 'vendor/autoload.php';

// Bootstrap the JMS custom annotations for Object to Json mapping
\Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace(
    'JMS\Serializer\Annotation',
    dirname(__DIR__).'/vendor/jms/serializer/src'
);
spl_autoload_register(function ($class) {
    if (strpos($class, 'Ftbl\\') > -1) {
    $class = str_replace("Ftbl\\", '', $class);
    $class = str_replace("\\", '/', $class);
    $className = __DIR__.'/../app/'.$class;
    $fileName = stream_resolve_include_path($className . '.php');
    if ($fileName !== false) {
        include_once $fileName;
    } else {
        throw new Exception('Cant find class '.$className);
    }
}
});

<?php
namespace Test;

use \Ftbl\Match\DTO\Match;
use \Ftbl\Match\DTO\Team;
use \Ftbl\Match\DTO\League;
use \Ftbl\Match\DTO\Result;
use \Ftbl\Match\DTO\Round;
use \Ftbl\Match\DTO\Stadium;

class MatchTest extends \PHPUnit_Framework_TestCase
{
    public function testPremierLeagueFixtures()
    {
        $match = new Match();
        $match->setKickoff('123');
        
        $match->setHomeTeam(new Team('Liverpool'));
        $match->setAwayTeam(new Team('Everton'));
        $match->setLeague(new League('england1'));
        $match->setFtResult(new Result(1,0));
        $match->setRound(new Round('Round 14'));
        $match->setStadium(new Stadium('Round 14'));
        $m = $match->toJson();
        var_dump($m);
        
        var_dump($match->fromJson($m));
    }


}